# Neverlan CTF (2020)

## Overview

* **CTF Time Link:** https://ctftime.org/event/971
* **Duration:** Sat, 08 Feb. 2020, 15:00 UTC — Tue, 11 Feb. 2020, 23:00 UTC
* **Team Mates:** TahSec team competition

```
Title                              Category             Points  Flag
---------------------------------- -------------------- ------- ---------------------------------------------------------------------------------------------------- 
Cookie Monster                     Web                  10      flag{YummyC00k13s}
Pigsfly                            Crypto               30      flag{d0wn_and_d1r7y
Stop the Bot                       Web                  50      flag{n0_b0ts_all0w3d}
SQL Breaker                        Web                  50      flag{Sql1nj3ct10n}
Follow Me!                         Web                  100     flag{d0nt_t3ll_m3_wh3r3_t0_g0}
password_crack                     Programming          100     orange-1984-zestyfe
Browser Bias                       Web                  150     flag{8b1t_w3b}
Robot Talk                         Programming          200     flag{Ant1_hum4n}
Baby RSA                           Crypto               250     flag{sm4ll_pr1m3s_ar3_t0_e4sy}
Reverse Engineer                   Reverse Engineering  300     flag{w3con7r0lth3b1nari3s}
```

---

## Cookie Monster

* **Category:** Web
* **Points:** 10

#### Challenge
Simple HTML page which only displayed the text "He's my favourite red guy".

#### Solution
1. Examined cookie storage for the page, found one cookie with name "Red_guys_name" and value "Name_goes_here".
2. Changed the value to "Elmo" and refreshed the page.
3. Flag displayed on screen.

#### Flag
```
flag{YummyC00k13s}
```
---

## Pigsfly

* **Category:** Crypto
* **Points:** 30

#### Challenge
![Pigsfly image](/images/pigsfly.png)

#### Solution
Looked like a substitution cipher. You can see that the "a" character in the "flag{" part of the text is re-used in the middle of the flag. I figured the middle word was probably "and", and this gives us the "n" and "d" characters. Substitute those throughout the flag and you can pretty much guess from there. Apparently this is "pigpen" cipher.

#### Flag
```
flag{d0wn_and_d1r7y}
```
---

## Stop the Bot

* **Category:** Web
* **Points:** 50

#### Challenge
A link to a website

#### Solution
Navigated to /robots.txt. Showed disallowed file flag.txt. Opened this file which displayed the flag.

#### Flag
```
flag{n0_b0ts_all0w3d}
```
---

## SQL Breaker

* **Category:** Web
* **Points:** 50

#### Challenge
Simple website with a login form.

#### Solution
I submitted username:
```
admin' OR ''='
```

And password:
```
password' OR ''='
```

This logged into the site as the admin user and displayed the flag.

#### Flag
```
flag{Sql1nj3ct10n} 
```
---

## Follow Me!

* **Category:** Web
* **Points:** 100

#### Challenge

Link to a website

#### Solution
Opening the link started a number of redirects. I recorded all of the redirects in Burp and manually looked at each of the responses. One of them (http://ql2yfq0kkm.neverlanctf.com/) contained the flag in plain text, before redirecting to a new site.

#### Flag
```
flag{d0nt_t3ll_m3_wh3r3_t0_g0}
```
---

## password_crack

* **Category:** Programming
* **Points:** 100

#### Challenge

```
Another day, another CTF challenge.

This one should be super straight forward. Before you go on, go read this article: https://thehackernews.com/2019/10/unix-bsd-password-cracked.html
Ok, did you read that article? Good. So your challenge is to crack a password. Just like Ken Thompson, our password will be in a 'known format'.

The format we'll use is: color-random_year-neverlan_team_member's_name. (all lowercase)

A sample password could be: red-1991-s7a73farm

Here's your hash: 267530778aa6585019c98985eeda255f. The hashformat is md5.
Useful Links: https://hashcat.net/wiki/doku.php?id=combinator_attack https://hashcat.net/forum/thread-7571.html

    -BashNinja

Your flag doesn't need to be in the normal flag{flagGoesHere} syntax
```

#### Solution
I wrote a python script to brute force different combinations until I found the flag. Neverlan team member names were found on their CTF Time team page.
```
import hashlib

the_hash = "267530778aa6585019c98985eeda255f"

colours = ["red", "orange", "yellow", "green", "blue", "purple", "brown", "magenta", "tan", "cyan", "olive", "maroon", "navy", "aquamarine", "turquoise", "silver", "lime", "teal", "indigo", "violet", "pink", "black", "white", "grey", "gray"]

team = ["zestyfe", "durkinza", "purvesta", "s7a73farm"]

for t in team:
	for c in colours:
		for y in range(0,3000):
			guess = c + "-" + str(y) + "-" + t
			result = hashlib.md5(guess.encode())
			if result.hexdigest() == the_hash:
				print(guess)
				quit()
```

#### Flag
```
orange-1984-zestyfe
```
---

## Browser Bias

* **Category:** Web
* **Points:** 150

#### Challenge
Given a link to a website which only displayed "Sorry, this site is only optimized for browsers that run on commodo 64"

#### Solution
1. Used BurpSuite to edit the User-Agent header to "commodo 64" and several other variants but this returned the same response.
2. I did a google search for "commodo 64 user agent" and found this site (https://gist.github.com/dstufft/2502524) which lists a whole bunch of user agents. The commodo 64 correct user agent header should be "Contiki/1.0". Swapping the user agent to this and re-requesting the page returned the flag.

#### Flag
```
flag{8b1t_w3b}
```
---

## Robot Talk

* **Category:** Programmin
* **Points:** 200

#### Challenge
```
challenges.neverlanctf.com:1120
```

#### Solution
Visiting the provided server displayed the following:

```
$ nc challenges.neverlanctf.com 1120
Welcome to the NeverLAN CTF.
You have 10 seconds to answer these questions.
decrypt: a3RxaGthd2hreQ==
```

We needed to decode the base64 text within the 10 second time limit. I wrote the following python script to solve the challenge:

```
import pwn
import base64

conn = pwn.remote('challenges.neverlanctf.com',1120)

for i in range(5):	
	print(conn.recvuntil(": "))
	q = conn.recv()
	a = base64.b64decode(q)
	conn.send(a)

print(conn.recvall())
```

Output:

```
python3 soln.py 
[+] Opening connection to challenges.neverlanctf.com on port 1120: Done
b'Welcome to the NeverLAN CTF.\nYou have 10 seconds to answer these questions.\ndecrypt: '
b'Awesome, continuing.\ndecrypt: '
b'Awesome, continuing.\ndecrypt: '
b'Awesome, continuing.\ndecrypt: '
b'Awesome, continuing.\ndecrypt: '
[+] Receiving all data: Done (37B)
[*] Closed connection to challenges.neverlanctf.com port 1120
b'Awesome, continuing.\nflag{Ant1_hum4n}'
```


#### Flag
```
flag{Ant1_hum4n
```
---

## Baby RSA

* **Category:** Crypto
* **Points:** 250

#### Challenge

```
We've intercepted this RSA encrypted message 2193 1745 2164 970 1466 2495 1438 1412 1745 1745 2302 1163 2181 1613 1438 884 2495 2302 2164 2181 884 2302 1703 1924 2302 1801 1412 2495 53 1337 2217 we know it was encrypted with the following public key e: 569 n: 2533
```

#### Solution
- We know that n = p \* q, which must be prime numbers. The only primes which are factors of 2533 are 17 and 149.
- Used this website (https://www.cryptool.org/en/cto-highlights/rsa-step-by-step) which allowed me enter our p, q and e values to encode text.
- Each number in the description was an encrypted letter. f -> 2193, l->1745, a->2164, g->970, etc.
- By entering in the decimal values of ASCII text, I decoded the flag letter by letter. Could have tried to automate the process but it didn't take long to do by hand.

#### Flag
```
flag{sm4ll_pr1m3s_ar3_t0_e4sy}
```
---

## Reverse Engineer

* **Category:** Reverse Engineering
* **Points:** 300

#### Challenge

Given a file named "revseng"

#### Solution
I opened the program in Ghidra and found a function named "print" with the following code:
```
void print(void)

{
  undefined *puVar1;
  char *flg;
  char rb;
  char lb;
  char g;
  char a;
  char l;
  char f;
  
  puVar1 = (undefined *)malloc(0x15);
  *puVar1 = 0x77;
  puVar1[1] = 0x33;
  puVar1[2] = 99;
  puVar1[3] = 0x6f;
  puVar1[4] = 0x6e;
  puVar1[5] = 0x37;
  puVar1[6] = 0x72;
  puVar1[7] = 0x30;
  puVar1[8] = 0x6c;
  puVar1[9] = 0x74;
  puVar1[10] = 0x68;
  puVar1[0xb] = 0x33;
  puVar1[0xc] = 0x62;
  puVar1[0xd] = 0x31;
  puVar1[0xe] = 0x6e;
  puVar1[0xf] = 0x61;
  puVar1[0x10] = 0x72;
  puVar1[0x11] = 0x69;
  puVar1[0x12] = 0x33;
  puVar1[0x13] = 0x73;
  puVar1[0x14] = 0;
  printf("%c%c%c%c%c%s%c\n",0x66,0x6c,0x61,0x67,0x7b,puVar1,0x7d);
  return;
}
```

I copied those hex values into an online hex to ascii tool and it printed the flag.

#### Flag
```
flag{w3con7r0lth3b1nari3s}
```


